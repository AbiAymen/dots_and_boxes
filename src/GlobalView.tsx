import React, { useState } from "react";
import { motion } from "framer-motion";
import {
  Typography,
  Button,
  Paper,
  Toolbar,
  TextField,
} from "@material-ui/core";
import SecondScreen from "./SecondScreen";
import Board from "./Board";
import firebase from "./Firebase";

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delay: 0.3,
      when: "beforeChildren",
      staggerChildren: 0.1,
    },
  },
};
const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
  },
};

export default function GlobalView() {
  const [screen, SetScreen] = useState(0);
  const [username, SetUsername] = useState("");
  const [game, Setgame] = useState("");

  const db = firebase.firestore();
  const PlayersRef = db.collection("players");
  const GamesRef = db.collection("games");

  let addPlayer = (name: string) => {
    PlayersRef.add({ name: name });
  };
  let addGame = (game: string) => {
    GamesRef.add({ game: { name: game, players: {} } });
  };

  return (
    <div>
      <motion.div
        className="container"
        variants={container}
        initial="hidden"
        animate="visible"
      >
        <Typography variant="h1" color="textPrimary">
          Dots and Boxes
        </Typography>
      </motion.div>
      <Paper
        style={{
          width: "50%",
          display: "flex",
          justifyContent: "space-around",
          margin: "auto",
          padding: "5px",
          alignItems: "center",
        }}
      >
        {screen === 0 ? (
          <motion.div
            className="container"
            variants={container}
            initial="hidden"
            animate="visible"
          >
            <motion.ul
              className="container"
              variants={container}
              initial="hidden"
              animate="visible"
            >
              <motion.div className="item" variants={item}>
                <Typography variant="h5" color="textPrimary">
                  Please enter your username
                </Typography>
              </motion.div>

              <motion.div className="item" variants={item}>
                <Toolbar />
              </motion.div>
              <motion.div className="item" variants={item}>
                <TextField
                  id="outlined-basic"
                  label="Username"
                  variant="outlined"
                  value={username}
                  onChange={(e) => {
                    SetUsername(e.target.value);
                  }}
                />
              </motion.div>
              <Toolbar />
            </motion.ul>
            <motion.div className="item" variants={item}>
              <Button
                variant="outlined"
                color="secondary"
                onClick={() => {
                  addPlayer(username);
                  SetScreen(1);
                }}
              >
                Next
              </Button>
            </motion.div>
          </motion.div>
        ) : screen === 1 ? (
          <motion.div
            className="container"
            variants={container}
            initial="hidden"
            animate="visible"
          >
            <SecondScreen />
            <motion.div className="item" variants={item}>
              <Toolbar />
            </motion.div>
            <motion.div className="item" variants={item}>
              <Typography variant="h5" color="textPrimary">
                Create new game
              </Typography>
              <Toolbar />
            </motion.div>
            <motion.div className="item" variants={item}>
              <TextField
                id="outlined-basic"
                label="Game name"
                variant="outlined"
                value={game}
                onChange={(e) => {
                  Setgame(e.target.value);
                }}
              />
            </motion.div>
            <motion.div className="item" variants={item}>
              <Button
                variant="outlined"
                color="secondary"
                onClick={() => {
                  addGame(game);
                  SetScreen(2);
                }}
              >
                Create
              </Button>
            </motion.div>
          </motion.div>
        ) : (
          <Board />
        )}
      </Paper>
    </div>
  );
}
