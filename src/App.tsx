import React from "react";
import "./App.css";
import GlobalView from "./GlobalView";

function App() {
  return (
    <div className="App">
      <GlobalView />
    </div>
  );
}

export default App;
