import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyCVGOdbSR6ZZoJTaX2vEu3CaaQzpoqirpk",
  authDomain: "dotsandboxes-da97e.firebaseapp.com",
  databaseURL: "https://dotsandboxes-da97e.firebaseio.com",
  projectId: "dotsandboxes-da97e",
  storageBucket: "dotsandboxes-da97e.appspot.com",
  messagingSenderId: "295263178696",
  appId: "1:295263178696:web:611f1325f3591240d7fafd",
  measurementId: "G-4PSKXN73E8",
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase;
