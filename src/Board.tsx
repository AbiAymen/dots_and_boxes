import React, { useState, useEffect } from "react";

let vertices = [
  [
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
  ],
  [
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
  ],
  [
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
  ],
  [
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
  ],
  [
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
    { h: 0, v: 0 },
  ],
];
let squarelist: { x: number; y: number }[] = [{ x: 5, y: 5 }];

let squareSearch = () => {
  console.log(vertices)

  for (var i = 0; i < 5; i++) {
    for (var j = 0; j < 5; j++) {
     
      if (
        vertices[i][j].h === 1 &&
        vertices[i][j].v === 1 &&
        vertices[i][j].h !== 2 &&
        vertices[i][j].v !== 2
      ) {
       

        if (vertices[i + 1][j].h !== 0 && vertices[i][j + 1].v !== 0) {
          console.log("square");
          vertices[i][j].h = 2;
          vertices[i][j].v = 2;
        }
      }
    }
  }
};

export default function Board() {
  const [stateVertices, SetStateVertices] = useState(vertices);
  const [changed, SetChanged] = useState(false);

  useEffect(() => {
    
    SetStateVertices(vertices);
    squareSearch()
    
    
  }, [changed]);

  return (
    <div style={{ height: "200px" }}>
      <svg style={{ height: "400px", width: "400px" }}>
        <svg>
          {Array.from(Array(5), (e, i) => {
            return (
              <svg key={`${i}`}>
                {Array.from(Array(i === 4 ? 4 : 5), (e, j) => {
                  let coordinate = `${80 * j + 10},${80 * i + 10} , ${
                    80 * j + 90
                  },${80 * i + 10}`;
                  let coordinate2 = `${80 * j + 10},${80 * i + 10} ,${
                    80 * j + 10
                  }, ${80 * i + 90}`;

                  return (
                    <svg key={`${stateVertices[i][j]}}+${i}+${j}`}>
                      {i === 4 ? (
                        <polyline
                          points={coordinate}
                          strokeWidth="6"
                          stroke={
                            stateVertices[i][j].h === 0 ? "black" : "blue"
                          }
                          onClick={() => {
                            vertices[i][j].h = 1;
                            squareSearch();
                            SetChanged(!changed);
                          }}
                        />
                      ) : j === 4 ? (
                        <polyline
                          points={coordinate2}
                          strokeWidth="6"
                          stroke={
                            stateVertices[i][j].v === 0 ? "black" : "blue"
                          }
                          onClick={() => {
                            vertices[i][j].v = 1;
                            squareSearch();
                            SetChanged(!changed);
                          }}
                        />
                      ) : (
                        <svg>
                          {" "}
                          <polyline
                            points={coordinate}
                            strokeWidth="6"
                            stroke={
                              stateVertices[i][j].h === 0 ? "black" : "blue"
                            }
                            onClick={() => {
                              vertices[i][j].h = 1;
                              squareSearch();
                              SetChanged(!changed);
                            }}
                          />
                          <svg style={{display:"flex", justifyContent:"flex-start"}}>
                          <polyline
                            points={coordinate2}
                            strokeWidth="6"
                            stroke={
                              stateVertices[i][j].v === 0 ? "black" : "blue"
                            }
                            onClick={() => {
                              vertices[i][j].v = 1;
                              squareSearch();
                              SetChanged(!changed);
                            }}
                          />
                          {stateVertices[i][j].h===2 ? <circle cx={80 * j + 50} cy={80 * i + 50} r="30px" color="red"/> : <div></div>}
                          </svg>
                        
                        </svg>
                      )}
                    </svg>
                  );
                })}
              </svg>
            );
          })}
        </svg>
      </svg>
    </div>
  );
}
