import React from "react";
import { motion } from "framer-motion";
import { Typography, Button, Paper } from "@material-ui/core";

const container = {
  hidden: { opacity: 1, scale: 0 },
  visible: {
    opacity: 1,
    scale: 1,
    transition: {
      delay: 0.3,
      when: "beforeChildren",
      staggerChildren: 0.1,
    },
  },
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
  },
};

const gameList = ["game 1", "game 2", "game 3"];

export default function SecondScreen() {
  return (
    <div>
      <motion.ul
        className="container"
        variants={container}
        initial="hidden"
        animate="visible"
      >
        {gameList.length !== 0 ? (
          <motion.div className="item" variants={item}>
            <Typography variant="h5" color="textPrimary">
              Game list
            </Typography>
            <motion.ul
              className="container"
              variants={container}
              initial="hidden"
              animate="visible"
            >
              {gameList.map((value, index) => {
                return (
                  <motion.div className="item" variants={item}>
                    <Paper
                      elevation={3}
                      style={{
                        display: "flex",
                        justifyContent: "space-around",
                        margin: "auto",
                        padding: "5px",
                        alignItems: "center",
                      }}
                    >
                      {value}
                      <Button variant="outlined" color="secondary">
                        Join
                      </Button>
                    </Paper>
                  </motion.div>
                );
              })}
            </motion.ul>
          </motion.div>
        ) : (
          <div>there is no games</div>
        )}
      </motion.ul>
    </div>
  );
}
